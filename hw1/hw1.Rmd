---
title: 'Analysis of Financial Time Series: HW1'
author: "Andrew Nguyen"
date: "Spring 2017"
output:
  pdf_document: default
  html_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE,message=FALSE, cache = TRUE)
cat<-read.table('d-cat0716-3dx.txt',header = T)
pg<-read.table('m-pg6116-3dx.txt',header = T)
ff<-read.table('d-ff-factors.txt',header = T)
library(fBasics)
library(ggplot2)
library(reshape2)
library(tseries)
library(ggthemes)
library(e1071)
library(dplyr)
library(Hmisc)
library(pander)
```

#1)
 
## a.    
```{r 1a}
pander(basicStats(cat)[c('Mean','Stdev','Skewness','Kurtosis','Minimum','Maximum'),c('RET','vwretd','ewretd','sprtrn')])
```
    
## b.
We reject the null hypotheses that any of RET, vwretd, ewretd, or sprtrn is normally distributed at a >99% confidence level. First we examine the density plots, and then note that the Jarque-Bera test statistic has a p-value less than .001 
    
```{r 1b}
    cat_melted<-melt(cat[c('RET','vwretd','ewretd','sprtrn')])
    ggplot(cat_melted, aes(value, colour = variable)) +
    geom_density() +
    ggtitle('CAT Return Density') +
    theme_tufte()
```

```{r 1b2}
pander(jarque.bera.test(cat$RET))
pander(jarque.bera.test(cat$vwretd))
pander(jarque.bera.test(cat$ewretd))
pander(jarque.bera.test(cat$sprtrn))
```

##c.
```{r 1c}
    cat$ln_ret <- log(cat$RET+1)
    cat$ln_vwretd <- log(cat$vwretd+1)
    cat$ln_ewretd <- log(cat$ewretd+1)
    cat$ln_sprtrn <- log(cat$sprtrn+1)
    
pander(basicStats(cat)[c('Mean','Stdev','Skewness','Kurtosis','Minimum','Maximum'),c('ln_ret','ln_vwretd','ln_ewretd','ln_sprtrn')])
```
   
   
##d.
We cannot reject the null hypothesis that the log returns for CAT and the log returns for S&P are statistically different from 0. 
```{r 1d}
pander(t.test(cat$ln_ret))
pander(t.test(cat$ln_sprtrn))
```

##e.
```{r 1e}
    cat_melted<-melt(cat[c('ln_ret','ln_ewretd')])
    ggplot(cat_melted, aes(value, colour = variable)) +
    geom_density() +
    ggtitle('CAT Log-Return Density') +
    theme_tufte()
```

\pagebreak

#2)
##a. 

```{r 2a}
pander(basicStats(pg)[c('Mean','Stdev','Skewness','Kurtosis','Minimum','Maximum'),c('RET','vwretd','ewretd','sprtrn')])
```

##b. 
We reject the null hypotheses that any of RET, vwretd, ewretd, or sprtrn is normally distributed at a >99% confidence level. First we examine the density plots, and then note that the Jarque-Bera test statistic has a p-value less than .001 

```{r 2b}
pg_melted<-melt(pg[c('RET','vwretd','ewretd','sprtrn')])
ggplot(pg_melted, aes(value, colour = variable)) +
  ggtitle('PG Return Density') +
  geom_density() +
  theme_tufte()
```
\pagebreak
```{r}
pander(jarque.bera.test(pg$RET))
pander(jarque.bera.test(pg$vwretd))
pander(jarque.bera.test(pg$ewretd))
pander(jarque.bera.test(pg$sprtrn))
```

##c.
```{r 2c}
pg$ln_ret <- log(pg$RET+1)
pg$ln_vwretd <- log(pg$vwretd+1)
pg$ln_ewretd <- log(pg$ewretd+1)
pg$ln_sprtrn <- log(pg$sprtrn+1)

pander(basicStats(pg)[c('Mean','Stdev','Skewness','Kurtosis','Minimum','Maximum'),c('ln_ret','ln_vwretd','ln_ewretd','ln_sprtrn')])
```


##d. 
We reject the null hypothesis that the log returns for PG and the log returns for S&P is 0 at a >99% confidence level. 
```{r 2d}
pander(t.test(pg$ln_ret))
pander(t.test(pg$ln_sprtrn))
```

##e.
```{r 2e}
pg_melted<-melt(pg[c('ln_ret','ln_ewretd')])
ggplot(pg_melted, aes(value, colour = variable)) +
  geom_density() +
  ggtitle('PG Log-Return Density') +
  theme_tufte()
```

#3)

## a.

```{r 3a}
pander(t.test(cat$ln_ret))

lowerci<-t.test(cat$ln_ret)$estimate-sd(cat$ln_ret)/sqrt(length(cat$ln_ret))*qnorm(.975)
upperci<-t.test(cat$ln_ret)$estimate+sd(cat$ln_ret)/sqrt(length(cat$ln_ret))*qnorm(.975)
mean<-t.test(cat$ln_ret)$estimate
```

[$`r as.numeric(lowerci)`$ , $`r upperci`$]

## b.

```{r 3b}
s_star<-skewness(cat$ln_ret)/sqrt(6/length(cat$ln_ret))
```

$$S^*= `r s_star`$$ 
$$Z_{.05/2}=`r qnorm(.975)`$$
So we cannot reject the null hypothesis that log returns for CAT are symmetric.

## c.
```{r 3c}
k_star<-(kurtosis(cat$ln_ret)-3)/sqrt(24/length(cat$ln_ret))
```
$$K^*= `r k_star`$$ 
$$Z_{.05/2}=`r qnorm(.975)`$$
So we reject the null hypothesis that log returns for CAT have normal tails.

# 4)


## a.

```{r 4a}
s_star<-skewness(cat$ln_sprtrn)/sqrt(6/length(cat$ln_sprtrn))
```

$$S^*= `r s_star`$$ 
$$Z_{.05/2}=`r qnorm(.975)`$$
So we reject the null hypothesis that log returns for SP are symmetric at a 95% level.

## b.
```{r 4b}
k_star<-(kurtosis(cat$ln_sprtrn)-3)/sqrt(24/length(cat$ln_sprtrn))
```
$$K^*= `r k_star`$$ 
$$Z_{.05/2}=`r qnorm(.975)`$$
So we reject the null hypothesis that log returns for CAT have normal tails.

## c.

```{r 4c}
pander(t.test(cat$ln_sprtrn))

lowerci<-t.test(cat$ln_sprtrn)$estimate-sd(cat$ln_sprtrn)/sqrt(length(cat$ln_sprtrn))*qnorm(.975)
upperci<-t.test(cat$ln_sprtrn)$estimate+sd(cat$ln_sprtrn)/sqrt(length(cat$ln_sprtrn))*qnorm(.975)
mean<-t.test(cat$ln_sprtrn)$estimate
```

[$`r as.numeric(lowerci)`$ , $`r upperci`$]

# 5)

##a.
```{r 5a}
ff_melted<-melt(ff,id.vars = c('date'))
ff_melted$dt<-as.Date(as.character(ff_melted$date),"%Y%m%d")
ggplot(data=filter(ff_melted,variable != 'umd')) +
  geom_line(aes(x=dt, y = value)) +
  facet_wrap(~variable,scales = "free_y")
```

##b.
We will use a single-sample t-test to test the null-hypothesis that the mean for each factor is 0. We do not reject the null hypothesis at a greater than 95% confidence-level.
```{r 5b}
pander(t.test(ff$smb))
pander(t.test(ff$hml))
pander(t.test(ff$mktrf))
pander(t.test(ff$umd))
```



##c.

### Correlation matrix for Pearson’s correlation coefficient: 
```{r 5c}
options(warn=-1)

pander(cor(as.matrix(ff[2:5]), method = c("pearson")))
```
### Correlation matrix for Kendall’s tau:
```{r }
pander(cor(as.matrix(ff[2:5]), method = c("kendall")))
```


### Correlation matrix for Spearman’s rho:
```{r }
pander(cor(as.matrix(ff[2:5]), method = c("spearman")))
```